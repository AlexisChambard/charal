<?php

namespace App\Controller\Admin;

use App\Entity\TrialType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_SUPER_ADMIN')]
class TrialTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TrialType::class;
    }
}
