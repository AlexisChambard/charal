<?php

namespace App\Controller\Admin;

use App\Entity\ObstacleNote;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[IsGranted('ROLE_LOCAL_ADMIN')]
class ObstacleNoteCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly EntityRepository $entityRepository,
    ) {
    }

    public function createIndexQueryBuilder(
        SearchDto $searchDto,
        EntityDto $entityDto,
        FieldCollection $fields,
        FilterCollection $filters
    ): QueryBuilder {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->entityRepository->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $response->leftJoin("entity.judge", "judge");
        $response->leftJoin("judge.competition", "competition");
        $response->andWhere("competition.admin = :user")->setParameter("user", $this->getUser());

        return $response;
    }

    public static function getEntityFqcn(): string
    {
        return ObstacleNote::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return Actions::new();
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('id'),
            AssociationField::new('obstacleConfiguration'),
            AssociationField::new('judge'),
            AssociationField::new('rider'),
            NumberField::new('total'),
            TextField::new('observation'),
        ];
    }
}
