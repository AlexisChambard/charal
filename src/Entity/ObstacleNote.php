<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ObstacleNoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: ObstacleNoteRepository::class)]
#[ApiResource(
    collectionOperations: ["POST"],
    itemOperations: ["GET"],
    denormalizationContext: ['groups' => ['note:write']],
    normalizationContext: ['groups' => ['note:read']],
)]
class ObstacleNote
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: 'uuid')]
    private Uuid $id;

    #[ORM\ManyToOne(targetEntity: ObstacleConfiguration::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['note:write'])]
    private ?ObstacleConfiguration $obstacleConfiguration;

    #[ORM\ManyToOne(targetEntity: Judge::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Judge $judge;

    #[ORM\ManyToOne(targetEntity: Rider::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['note:write'])]
    private ?Rider $rider;

    #[ORM\ManyToOne(targetEntity: NoteConfiguration::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['note:write'])]
    private ?NoteConfiguration $contractNote;

    #[ORM\ManyToOne(targetEntity: NoteConfiguration::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['note:write'])]
    private ?NoteConfiguration $styleNote;

    #[ORM\ManyToOne(targetEntity: NoteConfiguration::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['note:write'])]
    private ?NoteConfiguration $allureNote;

    #[ORM\ManyToMany(targetEntity: NoteConfiguration::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['note:write'])]
    private Collection $penaltiesNotes;

    #[ORM\ManyToMany(targetEntity: PenaltyConfiguration::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['note:write'])]
    private Collection $penalties;

    #[ORM\Column(type: 'text')]
    #[Groups(['note:write'])]
    private ?string $observation;

    public function getTotal()
    {
        $total = 0;

        $total += $this->contractNote->getValue();
        $total += $this->styleNote?->getValue() ?? 0;
        $total += $this->allureNote?->getValue() ?? 0;

        foreach ($this->penaltiesNotes as $penaltiesNote) {
            $total += $penaltiesNote->getValue();
        }

        if (count($this->penalties) !== 0) {
            $total = 0;
        }

        return $total;
    }

    public function __construct()
    {
        $this->penaltiesNotes = new ArrayCollection();
        $this->penalties = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @param Uuid $id
     * @return ObstacleNote
     */
    public function setId(Uuid $id): ObstacleNote
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Judge|null
     */
    public function getJudge(): ?Judge
    {
        return $this->judge;
    }

    /**
     * @param Judge|null $judge
     * @return ObstacleNote
     */
    public function setJudge(?Judge $judge): ObstacleNote
    {
        $this->judge = $judge;
        return $this;
    }

    /**
     * @return Rider|null
     */
    public function getRider(): ?Rider
    {
        return $this->rider;
    }

    /**
     * @param Rider|null $rider
     * @return ObstacleNote
     */
    public function setRider(?Rider $rider): ObstacleNote
    {
        $this->rider = $rider;
        return $this;
    }

    /**
     * @return NoteConfiguration|null
     */
    public function getContractNote(): ?NoteConfiguration
    {
        return $this->contractNote;
    }

    /**
     * @param NoteConfiguration|null $contractNote
     * @return ObstacleNote
     */
    public function setContractNote(?NoteConfiguration $contractNote): ObstacleNote
    {
        $this->contractNote = $contractNote;
        return $this;
    }

    /**
     * @return NoteConfiguration|null
     */
    public function getStyleNote(): ?NoteConfiguration
    {
        return $this->styleNote;
    }

    /**
     * @param NoteConfiguration|null $styleNote
     * @return ObstacleNote
     */
    public function setStyleNote(?NoteConfiguration $styleNote): ObstacleNote
    {
        $this->styleNote = $styleNote;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getPenaltiesNotes(): ArrayCollection|Collection
    {
        return $this->penaltiesNotes;
    }

    /**
     * @param ArrayCollection|Collection $penaltiesNotes
     * @return ObstacleNote
     */
    public function setPenaltiesNotes(ArrayCollection|Collection $penaltiesNotes): ObstacleNote
    {
        $this->penaltiesNotes = $penaltiesNotes;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getPenalties(): ArrayCollection|Collection
    {
        return $this->penalties;
    }

    /**
     * @param ArrayCollection|Collection $penalties
     * @return ObstacleNote
     */
    public function setPenalties(ArrayCollection|Collection $penalties): ObstacleNote
    {
        $this->penalties = $penalties;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getObservation(): ?string
    {
        return $this->observation;
    }

    /**
     * @param string|null $observation
     * @return ObstacleNote
     */
    public function setObservation(?string $observation): ObstacleNote
    {
        $this->observation = $observation;
        return $this;
    }

    /**
     * @return NoteConfiguration|null
     */
    public function getAllureNote(): ?NoteConfiguration
    {
        return $this->allureNote;
    }

    /**
     * @param NoteConfiguration|null $allureNote
     * @return ObstacleNote
     */
    public function setAllureNote(?NoteConfiguration $allureNote): ObstacleNote
    {
        $this->allureNote = $allureNote;
        return $this;
    }

    /**
     * @return ObstacleConfiguration|null
     */
    public function getObstacleConfiguration(): ?ObstacleConfiguration
    {
        return $this->obstacleConfiguration;
    }

    /**
     * @param ObstacleConfiguration|null $obstacleConfiguration
     * @return ObstacleNote
     */
    public function setObstacleConfiguration(?ObstacleConfiguration $obstacleConfiguration): ObstacleNote
    {
        $this->obstacleConfiguration = $obstacleConfiguration;
        return $this;
    }
}
