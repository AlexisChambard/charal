<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RiderRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: RiderRepository::class)]
#[UniqueEntity(fields: ['firstName', 'lastName', 'competition', 'licenceNumber'])]
#[ApiResource(
    collectionOperations: [],
    itemOperations: ["GET"],
    denormalizationContext: ['groups' => ['rider:write']],
    normalizationContext: ['groups' => ['rider:read', 'configuration:read']],
)]
class Rider
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: 'uuid')]
    #[Groups(['rider:read'])]
    private Uuid $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['rider:read'])]
    private ?string $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['rider:read'])]
    private ?string $lastName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['rider:read'])]
    private ?string $licenceNumber;

    #[ORM\Column(type: 'integer')]
    #[Groups(['rider:read'])]
    private ?int $bibNumber;

    #[ORM\ManyToOne(targetEntity: Competition::class, inversedBy: 'riders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Competition $competition;

    public function __toString(): string
    {
        return "$this->firstName $this->lastName";
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @param Uuid $id
     * @return Rider
     */
    public function setId(Uuid $id): Rider
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return Rider
     */
    public function setFirstName(?string $firstName): Rider
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return Rider
     */
    public function setLastName(?string $lastName): Rider
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLicenceNumber(): ?string
    {
        return $this->licenceNumber;
    }

    /**
     * @param string|null $licenceNumber
     * @return Rider
     */
    public function setLicenceNumber(?string $licenceNumber): Rider
    {
        $this->licenceNumber = $licenceNumber;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBibNumber(): ?int
    {
        return $this->bibNumber;
    }

    /**
     * @param int|null $bibNumber
     * @return Rider
     */
    public function setBibNumber(?int $bibNumber): Rider
    {
        $this->bibNumber = $bibNumber;
        return $this;
    }

    public function getCompetition(): ?Competition
    {
        return $this->competition;
    }

    public function setCompetition(?Competition $competition): self
    {
        $this->competition = $competition;

        return $this;
    }
}
