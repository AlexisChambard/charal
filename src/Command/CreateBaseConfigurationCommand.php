<?php

namespace App\Command;

use App\Entity\TrialType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create:config',
    description: 'Create basic configuration',
)]
class CreateBaseConfigurationCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Create TrialType
        foreach (["P.T.V", "P.A"] as $item) {
            $trialType = (new TrialType())->setName($item);
            $this->em->persist($trialType);
        }

        $ptv = $this->em->getRepository(TrialType::class)->findOneBy(['name' => "P.T.V"]);
        

        $data = [];

        return Command::SUCCESS;
    }
}
