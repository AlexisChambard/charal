<?php

namespace App\Security;

use App\Entity\Judge;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class ApiAuthenticator extends AbstractAuthenticator
{
    use TargetPathTrait;

    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('Authorization') && $request->headers->get('PhoneIdentifier');
    }

    public function authenticate(Request $request): Passport
    {
        $token = $request->headers->get('Authorization');
        $phoneIdentifier = $request->headers->get('PhoneIdentifier');

        if (!$token || !$phoneIdentifier) {
            throw new CustomUserMessageAuthenticationException('No token or identifier found');
        }

        $judge = $this->em->getRepository(Judge::class)->findOneBy(['token' => $token]);

        if (!$judge) {
            throw new CustomUserMessageAuthenticationException('Invalid token');
        }

        if (!$judge->getPhoneIdentifier()) {
            $judge->setPhoneIdentifier($phoneIdentifier);
        }

        $judge->setTokenUsed(true);
        $this->em->flush();

        if ($judge->getPhoneIdentifier() !== $phoneIdentifier) {
            throw new CustomUserMessageAuthenticationException('Invalid token');
        }

        return new SelfValidatingPassport(new UserBadge($token));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse(['message' => strtr($exception->getMessageKey(), $exception->getMessageData())],
            Response::HTTP_UNAUTHORIZED);
    }
}
